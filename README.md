# Instrumented Swarm

Expose Hyperswarm and DHT state.

Optionally expose http endpoints to query the state.

Can be used to provide Prometheus metrics.

## Install

```npm i instrumented-swarm```

## API

#### ```const instrumentedSwarm = new InstrumentedSwarm(swarm, { server })```

Create a new instrumented swarm, based on the passed-in hyperswarm.

`server` is an optional fastify server.
If passed in, endpoints will be added to expose information about the dht and swarm.

#### ```instrumentedSwarm.swarm```

The Hyperswarm instance

#### ```instrumentedSwarm.replSeed```

The seed to access the repl-swarm, if `launchRepl` was set to true

#### ```instrumentedSwarm.server```

The fastify server instance, if a `server` was passed in

#### ```instrumentedSwarm.publicKey```

The public key of the swarm, in hex format

#### ```instrumentedSwarm.dhtPort```

The port on which the dht is listening

#### ```instrumentedSwarm.dhtHost```

The host on which the dht is listening

#### ```instrumentedSwarm.connectionPort```

The port used to set up connections with other peers in the swarm

#### ```instrumentedSwarm.dhtNodes```

A map with an entry for each member of the DHT.

The keys are their dht-node id's in hex format.

The value is an object containing the basic information of that node (such as IP address and port)

#### ```instrumentedSwarm.peerInfos```

A map with an entry for each connected peer in the swarm.

The keys are their public keys in hex format.

The values are an object with basic information about the peer, such as
- `remoteHost` (ip address)
- `remotePort`

#### ```instrumentedSwarm.registerPrometheusMetrics (promClient)```
Registers swarm metrics on a Prometheus client, so they can be scraped.

`promClient`: a [prom-client](https://github.com/siimon/prom-client).
#### ```instrumentedSwarm.getMetrics()```

Returns basic metrics about the swarm and dht.

### Endpoints

If a server is passed in, the following endpoints are exposed:

#### ```GET /swarm/key```
The swarm's public key, in hex format

#### ```GET /swarm/peerinfo```

Returns the list of all peerInfo objects

Optionally accepts query parameters `port` and `host`, to filter out only those `peerInfo` objects with corresponding `remoteHost` and `remotePort`

#### ```GET /swarm/peerinfo/:publicKey```

Returns the peerInfo object corresponding with the `publicKey`, or 404 if not found

#### ```GET /swarm/summary```

Returns an object with an entry per entry of `instrumentedSwarm.getMetrics()`

#### ```GET /dht/peer```

Returns a list of dht nodes as defined in `instrumentedSwarm.dhtNodes`
