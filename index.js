const b4a = require('b4a')
const { id: getDhtId } = require('dht-rpc/lib/peer.js')

class InstrumentedSwarm {
  constructor (swarm, { server } = {}) {
    this.swarmConnectionsOpened = 0
    this.swarmConnectionsClosed = 0

    this.swarm = swarm
    this.swarm.on('connection', conn => {
      this.swarmConnectionsOpened++
      conn.on('close', () => this.swarmConnectionsClosed++)
    })

    this.server = server ? addEndpoints(this, server) : null
  }

  get serverPort () {
    return this.server?.addresses()[0].port
  }

  get publicKey () {
    return b4a.toString(this.swarm.keyPair.publicKey, 'hex')
  }

  get dhtPort () {
    return this.swarm.dht.port
  }

  get dhtHost () {
    return this.swarm.dht.host
  }

  get connectionPort () {
    // TODO: check if always the same over all connections
    return [...this.peerInfos.values()][0]?.ownPort
  }

  get peers () {
    return this.swarm.peers
  }

  get connections () {
    return this.swarm.connections
  }

  get dhtNodes () {
    const res = new Map()
    for (const n of this.swarm.dht.nodes.toArray()) {
      const hexKey = b4a.toString(n.id, 'hex')
      const nodeInfo = {}
      for (const prop of Object.keys(n)) {
        // Get rid of references of ordered set datastruct
        if (prop !== 'next' && prop !== 'prev') {
          nodeInfo[prop] = n[prop]
        }
      }
      if (nodeInfo.id) nodeInfo.id = b4a.toString(nodeInfo.id, 'hex')
      res.set(hexKey, nodeInfo)
    }

    return res
  }

  get peerInfos () {
    const infos = new Map()

    for (const connection of this.connections) {
      const publicKey = b4a.toString(connection.remotePublicKey, 'hex')
      const peerInfo = this.peers.get(publicKey)
      const dhtId = b4a.toString(getDhtId(connection.rawStream.remoteHost, connection.rawStream.remotePort), 'hex')

      // TODO: verify that, if a node is present on the DHT,
      // it can always be found this way
      const dhtInfo = this.dhtNodes.get(dhtId)

      const info = {
        remoteHost: connection.rawStream.remoteHost,
        remotePort: connection.rawStream.remotePort,
        ownPort: connection.rawStream.socket._port, // TODO: use non-private accessor
        publicKey,
        banned: peerInfo.banned,
        priority: peerInfo.priority,
        client: peerInfo.client,
        topics: peerInfo.topics.map(t => b4a.toString(t, 'hex')),
        onDht: dhtInfo != null
      }

      infos.set(publicKey, info)
    }

    return infos
  }

  getMetrics () {
    const res = {}
    res.hyperswarm = {
      nrPeers: this.peers.size,
      nrHosts: this.nrSwarmHosts,
      connectionsOpened: this.swarmConnectionsOpened,
      connectionsClosed: this.swarmConnectionsClosed
    }
    res.dht = {
      nrEntries: this.nrDhtEntries,
      nrHosts: this.nrDhtHosts
    }

    // This should be translated to some measure of actual time
    // const nrTicksConnectedDhtOverview = new Map()
    // for (const [hexKey, node] of dhtNodes) {
    //   nrTicksConnectedDhtOverview.set(hexKey, node.pinged - node.added)
    // }

    return res
  }

  get nrSwarmHosts () {
    const uniques = new Set()
    for (const connection of this.connections) {
      uniques.add(connection.rawStream.remoteHost)
    }
    return uniques.size
  }

  get _tableEntries () {
    return this.swarm.dht.table.toArray()
  }

  get nrDhtEntries () {
    return this._tableEntries.length
  }

  get nrDhtHosts () {
    const hosts = new Set()
    for (const node of this._tableEntries) hosts.add(node.host)
    return hosts.size
  }

  get dhtBucketSizes () {
    const table = this.swarm.dht.table
    const bucketSizes = table.rows.map(r => r.nodes.length || 0)
    return bucketSizes
  }

  registerPrometheusMetrics (promClient) {
    registerPrometheusMetrics(this, promClient)
  }
}

function addEndpoints (instrumentedSwarm, server) {
  server.get('/swarm/key', async function (req, reply) {
    const key = b4a.toString(
      instrumentedSwarm.swarm.keyPair.publicKey,
      'hex'
    )
    reply.send(key)
  })

  server.get('/swarm/peerinfo', async function (req, reply) {
    const { port, host } = req.query

    const res = []
    for (const entry of instrumentedSwarm.peerInfos.values()) {
      const portOk = !port || `${entry.remotePort}` === port
      const hostOk = !host || entry.remoteHost === host
      if (portOk && hostOk) res.push(entry)
    }
    reply.send(res)
  })

  server.get('/swarm/peerinfo/:publicKey', async function (req, reply) {
    const { publicKey } = req.params

    const info = instrumentedSwarm.peerInfos.get(publicKey)
    if (!info) {
      reply.status(404)
    } else {
      reply.send(info)
    }
  })

  server.get('/swarm/summary', async function (req, reply) {
    reply.send(instrumentedSwarm.getMetrics())
  })

  server.get('/dht/peer', async function (req, reply) {
    const res = []
    for (const entry of instrumentedSwarm.dhtNodes.values()) {
      res.push(entry)
    }
    reply.send(res)
  })

  return server
}

function registerPrometheusMetrics (instrumentedSwarm, promClient) {
  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'hyperswarm_nr_peers',
    help: 'Number of peers this swarm is connected to',
    collect () {
      this.set(instrumentedSwarm.peers.size)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'hyperswarm_nr_hosts',
    help: 'Number of ip addresses the swarm is connected with (multiple ports on the same IP are counted only once)',
    collect () {
      this.set(instrumentedSwarm.nrSwarmHosts)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'dht_nr_entries',
    help: 'Number DHT entries held in the local table',
    collect () {
      this.set(instrumentedSwarm.nrDhtEntries)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'dht_nr_hosts',
    help: 'Number of ip addresses the dht holds entries from',
    collect () {
      this.set(instrumentedSwarm.nrDhtHosts)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'hyperswarm_connections_opened',
    help: 'Total number of connection opened by the swarm',
    collect () {
      this.set(instrumentedSwarm.swarmConnectionsOpened)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'hyperswarm_connections_closed',
    help: 'Total number of connection closed by the swarm',
    collect () {
      this.set(instrumentedSwarm.swarmConnectionsClosed)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'hyperswarm_nr_connections',
    help: 'Total number of open connections',
    collect () {
      this.set(instrumentedSwarm.connections.size)
    }
  })

  new promClient.Gauge({ // eslint-disable-line no-new
    name: 'hyperswarm_nr_connecting',
    help: 'Total number of connections in the process of being opened',
    collect () {
      this.set(instrumentedSwarm.swarm.connecting)
    }
  })

  const nrKademliaBuckets = 20
  const kademliaBuckets = []
  for (let i = 0; i < nrKademliaBuckets; i++) kademliaBuckets.push(i)

  new promClient.Histogram({ // eslint-disable-line no-new
    name: 'dht_kademlia_buckets',
    help: 'Amount of entries per Kademlia k-bucket',
    buckets: kademliaBuckets,
    async collect () {
      const bucketSizes = instrumentedSwarm.dhtBucketSizes
      const count = bucketSizes.reduce((prev, curr) => prev + curr)
      this.hashMap[''].bucketValues = bucketSizes
      this.hashMap[''].sum = count
      this.hashMap[''].count = count
    }
  })
}

module.exports = InstrumentedSwarm
